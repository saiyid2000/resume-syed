import { FaGitlab } from "react-icons/fa6";
import { CiLocationOn } from "react-icons/ci";
import { IoLogoInstagram } from "react-icons/io";
import { MdOutlineEmail, MdOutlineLocalPhone } from "react-icons/md";

const Sideber = () => {
  return (
    <div>
      <img
        src="https://scontent.fhdy2-1.fna.fbcdn.net/v/t39.30808-6/345301571_186126444362509_1260535884743714644_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=efb6e6&_nc_eui2=AeFsPOR-xB8-0DEGTygfvwUVeGTw16Okfyh4ZPDXo6R_KJWpaEtgbTCmLtOvbR5i2S06sLLAKYuipJu--qshHbIp&_nc_ohc=_6AKlF1Vli4AX9x5t2H&_nc_ht=scontent.fhdy2-1.fna&oh=00_AfA44ov8y-ggR3r6OzmlHmAsD4yklhVIOoxsTS9Zsyn_-g&oe=657D7F52"
        alt="syed"
        className="w-32 h-32 mx-auto rounded-full"
      />
      <h3 className="my-4 text-2xl font-medium tracking-wider font-rubik">
        <span>Syedabdulrohman Al-idrus</span>
      </h3>
      <p className="px-2 py-1 my-3 bg-gray-200 rounded-full">
        SOFTWARE ENGINEER / FRONT END DEVELOPER
      </p>

      {/* social icon*/}
      <div className="flex justify-evenly w-9/12 my-5 md:w-full mx-auto">
        <a href="">
          <FaGitlab className="w-8 h-8 cursor-pointer text-purple-500" />
        </a>
        <a href="">
          <IoLogoInstagram className="w-8 h-8 cursor-pointer text-purple-500" />
        </a>
      </div>

      {/* oddress */}
      <div
        className="py-4 my-5 bg-gray-200"
        style={{ marginLeft: "-1rem", marginRight: "-1rem" }}
      >
        <div className="m-2 flex items-center justify-center space-x-2">
          <CiLocationOn className="w-6 h-6" />
          <span>115/4 No.2 Yamu, Yaring ,Pattani, 94150</span>
        </div>

        <div className="flex items-center justify-center space-x-2">
          <MdOutlineEmail className="m-2 w-6 h-6 flex items-center justify-center space-x-2 " />
          <p>saiyid2000@gmail.coom</p>
        </div>

        <div className="flex items-center justify-center space-x-2">
          <MdOutlineLocalPhone className="m-2 w-6 h-6" /> <p>063-0803831</p>
        </div>
      </div>

      {/* Button */}
      <button
        className="my-2 bg-gradient-to-r from-violet-600 to-violet-300 w-8/12 rounded-full py-2 px-5 text-white"
        onClick={() => window.open("mailto:saiyid2000@gmail.coom")}
      >
        Email me
      </button>
      <button
        className="my-2 bg-gradient-to-r from-violet-600 to-violet-300 w-8/12 rounded-full py-2 px-5 text-white"
        onClick={() =>
          window.open("https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=152s")
        }
      >
        Favorite song
      </button>
    </div>
  );
};

export default Sideber;
