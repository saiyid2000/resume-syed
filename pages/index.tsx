import { useRouter } from "next/router";
import Homepage from "@/scenes/Homepage";

export default function Home() {
  const router = useRouter();
  return <Homepage />;
}
