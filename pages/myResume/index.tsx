import { useRouter } from "next/router";
import MyResume from "@/scenes/MyResume";

export default function Resume() {
  const router = useRouter();
  return <MyResume />;
}
