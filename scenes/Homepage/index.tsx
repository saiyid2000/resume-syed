import Link from "next/link";
import React from "react";
import Particles from "@/components/particles";
import { homepage } from "./constant";

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center w-screen h-screen overflow-hidden bg-gradient-to-tl from-black via-zinc-600/20 to-black">
      <div className="lg:mb-[12rem] z-10 max-w-5xl lg:w-full items-center justify-between font-mono text-sm lg:flex animate-fade-in">
        <p className="text-white">Syedabdulrohman Al-idrus</p>
      </div>
      <div className="hidden w-screen h-px animate-glow md:block animate-fade-left bg-gradient-to-r from-zinc-300/0 via-zinc-300/50 to-zinc-300/0" />
      <Particles
        className="absolute inset-0 -z-10 animate-fade-in"
        quantity={100}
      />
      <h1 className="font-semibold z-10 text-4xl text-transparent duration-1000 bg-white cursor-default text-edge-outline animate-title font-display sm:text-6xl md:text-9xl whitespace-nowrap bg-clip-text ">
        PORTFOLIO
      </h1>

      <div className="hidden w-screen h-px animate-glow md:block animate-fade-right bg-gradient-to-r from-zinc-300/0 via-zinc-300/50 to-zinc-300/0" />
      <div className="lg:my-16 text-center animate-fade-in">
        <div className="mt-16 grid text-center gap-8 justify-evenly lg:max-w-5xl lg:w-full lg:mb-0 lg:grid-cols-3 lg:gap-[12rem] lg:text-left">
          {homepage.map((item, index) => (
            <Link key={index} href={item.href}>
              <div className="group text-white rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30">
                <p className={`mb-3 text-2xl font-semibold`}>
                  {item.text1}{" "}
                  <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
                    -&gt;
                  </span>
                </p>
                <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
                  {item.text2}
                </p>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}
