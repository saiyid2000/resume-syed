import Particles from "@/components/particles";
import Sideber from "@/components/Sideber";
import Nav from "@/components/nav";
import Resume from "@/scenes/Resume";

export default function MyResume() {
  return (
    <main>
      <div className="grid grid-cols-12 gap-6 px-5 lg:px-48 my-20 sm:px-20 md:px-32">
        <Particles
          className="absolute inset-0 -z-10 animate-fade-in"
          quantity={100}
        />
        <div className="text-black col-span-12 p-4 text-center bg-white lg:col-span-3 rounded-2xl">
          <Sideber />
        </div>
        <div className="text-black col-span-12 p-4 text-center bg-white lg:col-span-9 rounded-2xl">
          <Nav />
          <Resume />
        </div>
      </div>
    </main>
  );
}
